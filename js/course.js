/**
 * Javascript file for the course website
 * Author: Johnny Tsheke
 */

function displayLastUpdate(){
	var upd=document.lastModified;
	$("#lastupdate").html(upd);
}

$(document).ready(function(){
	displayLastUpdate();
});